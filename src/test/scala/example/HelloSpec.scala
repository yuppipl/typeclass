package example

import org.scalatest._
import spire.implicits._

class HelloSpec extends FunSuite with Matchers {
  test("add") {
    Hello.add[Int](1, 2) shouldEqual 3
    Hello.add(1.2, 2.1) shouldEqual 3.3
    Hello.add(BigInt("123"), BigInt("1")) shouldEqual BigInt("124")
  }

  test("add2") {
    Hello.add2[Int](1, 2)(PlusInstances.intPlus) shouldEqual 3
    Hello.add2(1.2, 2.1)(PlusInstances.doublePlus) shouldEqual 3.3
    Hello.add2(BigInt("123"), BigInt("1"))(PlusInstances.bigIntPlus) shouldEqual BigInt("124")
  }

  test("add3") {
    import PlusInstances._
    Hello.add3[Int](1, 2) shouldEqual 3
    Hello.add3(1.2, 2.1) shouldEqual 3.3
    Hello.add3(BigInt("123"), BigInt("1")) shouldEqual BigInt("124")
  }

  test("add4") {
    import PlusInstances._
    Hello.add4[Int](1, 2) shouldEqual 3
    Hello.add4(1.2, 2.1) shouldEqual 3.3
    Hello.add4(BigInt("123"), BigInt("1")) shouldEqual BigInt("124")
  }

  test("add5") {
    import PlusInstances._
    Hello.add5[Int](1, 2) shouldEqual 3
    Hello.add5(1.2, 2.1) shouldEqual 3.3
    Hello.add5(BigInt("123"), BigInt("1")) shouldEqual BigInt("124")
  }

  test("add6") {
    import PlusInstances._
    Hello.add6[Int](1, 2) shouldEqual 3
    Hello.add6(1.2, 2.1) shouldEqual 3.3
    Hello.add6(BigInt("123"), BigInt("1")) shouldEqual BigInt("124")
  }

  test("add touples") {
    import PlusInstances._
    Hello.add6((1, 2), (2, 2)) shouldEqual(3, 4)
    Hello.add6((1.1, 2.1), (2.1, 2.1)) shouldEqual(3.2, 4.2)
    Hello.add6((1, 2.1), (2, 2.1)) shouldEqual(3, 4.2)
  }
}
