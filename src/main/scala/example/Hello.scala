package example

import spire.implicits._
import spire.math._

object Hello extends App {
  println("Hello")

  def addInt(a1: Int, a2: Int): Int = a1 + a2

  def addDouble(a1: Double, a2: Double): Double = a1 + a2

  def addBigInt(a1: BigInt, a2: BigInt): BigInt = a1 + a2

  def add[A: Numeric](a1: A, a2: A): A = a1 + a2

  def add2[A](a1: A, a2: A)(plus: Plus[A]): A = plus.plus(a1, a2)

  def add3[A](a1: A, a2: A)(implicit plus: Plus[A]): A = plus.plus(a1, a2)

  def add4[A: Plus](a1: A, a2: A): A = implicitly[Plus[A]].plus(a1, a2)

  def add5[A: Plus](a1: A, a2: A): A = Plus[A].plus(a1, a2)

  import PlusSyntax._

  def add6[A: Plus](a1: A, a2: A): A = a1 + a2

}

trait Plus[A] {
  def plus(a1: A, a2: A): A
}

object Plus {
  def apply[A: Plus]: Plus[A] = implicitly[Plus[A]]
}

object PlusInstances {
  implicit val intPlus = new Plus[Int] {
    override def plus(a1: Int, a2: Int): Int = a1 + a2
  }

  implicit val doublePlus = new Plus[Double] {
    override def plus(a1: Double, a2: Double): Double = a1 + a2
  }

  implicit val bigIntPlus = new Plus[BigInt] {
    override def plus(a1: BigInt, a2: BigInt): BigInt = a1 + a2
  }

  implicit def tuples[A: Plus, B: Plus] = new Plus[(A, B)] {

    import PlusSyntax._

    override def plus(a1: (A, B), a2: (A, B)): (A, B) = (a1._1 + a2._1, a1._2 + a2._2)
  }
}

object PlusSyntax {

  implicit class PlusOps[A: Plus](a1: A) {
    def +(a2: A): A = Plus[A].plus(a1, a2)
  }

}



